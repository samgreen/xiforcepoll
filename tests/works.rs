extern crate failure;
extern crate inject;
extern crate vigem;
extern crate winapi;
extern crate winproc;

use std::{env, mem, path::PathBuf};

use failure::Error;
use winapi::um::xinput::{XInputGetState, XINPUT_STATE};
use winproc::Process;

fn try_panic<R, F: FnOnce() -> Result<R, Error>>(f: F) -> R {
    match f() {
        Ok(v) => v,
        Err(e) => panic!("{}", e),
    }
}

fn xinput_state() -> XINPUT_STATE {
    unsafe {
        let mut state: XINPUT_STATE = mem::zeroed();
        XInputGetState(0, &mut state);
        state
    }
}

#[test]
fn works() {
    try_panic(|| {
        let process = Process::current();

        let manifest_dir = PathBuf::from(env::var("CARGO_MANIFEST_DIR")?);
        let dll_path = manifest_dir.join(r"target\release\xiforcepoll.dll");
        assert!(
            dll_path.is_file(),
            "DLL does not exist. Must build in release mode prior to testing."
        );

        inject::inject_dll(&dll_path, &process)?;
        assert!(
            inject::is_process_injected(&dll_path, &process)?,
            "DLL was not injected."
        );

        let mut connection = vigem::Connection::new()?;
        let (tx, _rx) = connection.add_target::<vigem::device::X360>()?;
        let mut cntr = tx.into_reporter();

        let mut state = xinput_state();
        assert_eq!(state.dwPacketNumber, 1);
        assert_eq!(state.Gamepad.bLeftTrigger, 0);

        state = xinput_state();
        assert_eq!(state.dwPacketNumber, 2);
        assert_eq!(state.Gamepad.bLeftTrigger, 0);
        cntr.update_with(|s| s.left_trigger += 1)?;

        state = xinput_state();
        assert_eq!(state.dwPacketNumber, 3);
        assert_eq!(state.Gamepad.bLeftTrigger, 1);

        Ok(())
    })
}

#![feature(integer_atomics)]

#[macro_use]
extern crate detour;
extern crate winapi;
#[macro_use]
extern crate lazy_static;
extern crate winproc;
#[macro_use]
extern crate const_cstr;

use std::{
    mem,
    sync::atomic::{AtomicU32, Ordering},
};

use detour::StaticDetour;
use std::{panic::catch_unwind, process};
use winapi::{
    shared::minwindef::{BOOL, DWORD, HINSTANCE, LPVOID, TRUE},
    um::{libloaderapi::GetProcAddress, winnt::DLL_PROCESS_ATTACH, xinput::XINPUT_STATE},
};
use winproc::Process;

static XI_DLL_NAMES: [&str; 5] = [
    "xinput1_4.dll",
    "xinput1_3.dll",
    "xinput1_2.dll",
    "xinput1_1.dll",
    "xinput9_1_0.dll",
];

const_cstr! {
    XINPUT_FN_NAME = "XInputGetState";
}

type XInputFn = unsafe extern "system" fn(DWORD, *mut XINPUT_STATE) -> DWORD;

static_detours! {
    struct XI1_4Hook: unsafe extern "system" fn(DWORD, *mut XINPUT_STATE) -> DWORD;
    struct XI1_3Hook: unsafe extern "system" fn(DWORD, *mut XINPUT_STATE) -> DWORD;
    struct XI1_2Hook: unsafe extern "system" fn(DWORD, *mut XINPUT_STATE) -> DWORD;
    struct XI1_1Hook: unsafe extern "system" fn(DWORD, *mut XINPUT_STATE) -> DWORD;
    struct XI9_1_0Hook: unsafe extern "system" fn(DWORD, *mut XINPUT_STATE) -> DWORD;
}

static XINPUT_PACKET_COUNTER: AtomicU32 = AtomicU32::new(1);

lazy_static! {
    static ref XINPUT_DETOURS: Vec<StaticDetour<XInputFn>> = hook_xinputs();
}

fn hook_xinputs() -> Vec<StaticDetour<XInputFn>> {
    let process = Process::current();
    let xi_mods: Vec<_> = process
        .module_entries()
        .expect("Failed to retrieve modules while hooking XInput.")
        .collect();
    XI_DLL_NAMES
        .iter()
        .zip([&XI1_4Hook, &XI1_3Hook, &XI1_2Hook, &XI1_1Hook, &XI9_1_0Hook].into_iter())
        .map(|(&dll_name, hook_init)| {
            xi_mods
                .iter()
                .find(|module| module.name.to_lowercase() == dll_name)
                .map(|module| unsafe {
                    let orig_fn = mem::transmute::<_, XInputFn>(GetProcAddress(
                        module.hmodule,
                        XINPUT_FN_NAME.as_ptr(),
                    ));
                    let mut hook = hook_init
                        .initialize(orig_fn, |_, _| 0)
                        .expect("Failed to initialize XInput hook.");
                    let tramp = mem::transmute::<_, XInputFn>(hook.trampoline());
                    hook.set_detour(move |i, state| {
                        let pc = XINPUT_PACKET_COUNTER.fetch_add(1, Ordering::Relaxed);
                        let res = tramp(i, state);
                        (*state).dwPacketNumber = pc;
                        res
                    });
                    hook.enable().expect("Failed to enable XInput hook.");
                    hook
                })
        })
        .filter_map(|h| h)
        .collect()
}

//#[allow(non_snake_case)]
//unsafe extern "system" fn XInputGetStateHook(dwUserIndex: DWORD, pState: *mut XINPUT_STATE) -> DWORD {
//    let mut pc = XINPUT_PACKET_COUNTER.lock().unwrap();
//    (*pState).dwPacketNumber = *pc;
//    let res = mem::transmute::<_, XInputFn>(XINPUT_DETOUR.trampoline())(dwUserIndex, pState);
//    *pc += 1;
//    res
//}

#[no_mangle]
#[allow(non_snake_case, unused_variables)]
pub extern "system" fn DllMain(
    dll_module: HINSTANCE,
    call_reason: DWORD,
    reserved: LPVOID,
) -> BOOL {
    match call_reason {
        DLL_PROCESS_ATTACH => {
            if let Err(_) = catch_unwind(|| lazy_static::initialize(&XINPUT_DETOURS)) {
                eprintln!("Error while hooking xinput. Exiting.");
                process::exit(1);
            }
        }
        //        DLL_PROCESS_DETACH => (),
        _ => (),
    }

    TRUE
}
